function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}
function divHtmlTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    sporocilo = klepetApp.zamenjajSmeskote(sporocilo);
    sporocilo = klepetApp.grdeBesede(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    if(sporocilo.indexOf("<img src=") > -1) {
      $('#sporocila').append(divHtmlTekst(sporocilo));
    } else {
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));      
    }
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var ime;
  var imeKanala;
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    ime = rezultat.vzdevek;
    
    $('#kanal').text(ime + " @ " + imeKanala);
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    imeKanala = rezultat.kanal;
    $('#kanal').text(ime + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    //if(sporocilo.indexOf("<img src=") > -1) {
      var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
      $('#sporocila').append(novElement);
   /* } else {
      var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
      $('#sporocila').append(novElement);
    }*/
    
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});