var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

Klepet.prototype.zamenjajSmeskote = function(sporocilo) {

  sporocilo = sporocilo.replace(/\:\)/g,"<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png' />");
  sporocilo = sporocilo.replace(/\;\)/g,"<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png' />");
  sporocilo = sporocilo.replace(/\(\y\)/g,"<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png' />");
  sporocilo = sporocilo.replace(/\:\*/g,"<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png' />");
  sporocilo = sporocilo.replace(/\:\(/g,"<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png' />");
  return sporocilo;
  
}
Klepet.prototype.grdeBesede = function(sporocilo) {
 /* var fs = require('fs');
  var array = fs.readFileSync('ois-dn2/public/swearWords.txt').toString().split("\n");
  for(var i in array) {
    sporocilo = sporocilo.replace(array[i], "***");
  }
  return sporocilo;*/
  var besedeSporocila = new Array("anal","anus","arse","ass","ballsack","balls","bastard","bitch","biatch","bloody","blowjob","blow job","bollock","bollok","boner","boob","bugger","bum","butt","buttplug","clitoris","cock","coon","crap","cunt","damn","dick","dildo","dyke","fag","feck","fellate","fellatio","felching","fuck","f u c k","fudgepacker","fudge packer","flange","Goddamn","God damn","hell","homo","jerk","jizz","knobend","knob end","labia","lmao","lmfao","muff","nigger","nigga","omg","penis","piss","poop","prick","pube","pussy","queer","scrotum","sex","shit","s hit","sh1t","slut","smegma","spunk","tit","tosser","turd","twat","vagina","wank","whore","wtf");
  
  var sporociloT = sporocilo.split(" ");
  
 /* var tabela = new Array();
  $.get('public/swearWords.txt', function(data){
    tabela = data.split('\n');
    console.log(tabela);
  });
  for(i in tabela) {
    sporocilo = sporocilo.replace(tabela[i], "***");
  }*/
  for (j in sporociloT) {
    for(i in besedeSporocila) {
      var locilo = sporociloT[j].substr(sporociloT[j].length - 1);
      if (sporociloT[j].toString().toLowerCase() == besedeSporocila[i].toString().toLowerCase()) {
        var zvezdice = "*";
        for (k=1; k<sporociloT[j].length; k++) {
          zvezdice = zvezdice + "*";
        }
        sporociloT[j] = sporociloT[j].toString().toLowerCase().replace(besedeSporocila[i].toString().toLowerCase(), zvezdice);
      }
      else if (locilo == "," || locilo == "." || locilo == ";" || locilo == "!" || locilo == "?" || locilo == "-" || locilo == "\"" || locilo == ")" || locilo == "\'" || locilo == "+" || locilo == "=") {
        if (sporociloT[j].substring(0,sporociloT[j].length-1).toString().toLowerCase() == besedeSporocila[i].toString().toLowerCase()) {
          var zvezdice = "*";
          for (k=2; k<sporociloT[j].length; k++) {
            zvezdice = zvezdice + "*";
          }
          sporociloT[j] = sporociloT[j].toString().toLowerCase().replace(besedeSporocila[i].toString().toLowerCase(), zvezdice);
        }
      }
    }
  }
  sporocilo = sporociloT.join(" ");
  return sporocilo;

}